import { Component, Input } from '@angular/core';
import { Passager } from '../../models/passager.model';
import { MatIcon } from '@angular/material/icon';
import { ClasseVolDirective } from "../../directives/passager/classe-vol.directive";
import { PoidsBagageDirective } from "../../directives/passager/poids-bagage.directive";
import { MatTooltipModule } from "@angular/material/tooltip";

@Component({
  selector: 'app-passager',
  standalone: true,
  imports: [
    MatIcon,
    ClasseVolDirective,
    PoidsBagageDirective,
    MatTooltipModule
  ],
  templateUrl: './passager.component.html',
  styleUrls: ['./passager.component.scss']
})
export class PassagerComponent {
  @Input({ required: true }) passager!: Passager;
  @Input() showPhotos: boolean = false;
}
