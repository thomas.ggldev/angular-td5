import { Component, OnInit } from '@angular/core';
import { FiltresComponent } from '../filtres/filtres.component';
import { ListeVolsComponent } from '../liste-vols/liste-vols.component';
import { ListePassagersComponent } from '../liste-passagers/liste-passagers.component';
import { IFiltres } from '../../models/filtres.model';
import { NgIf } from '@angular/common';
import { Vol } from '../../models/vol.model';
import { VolService } from '../../services/vol.service';
import { PassagerService } from '../../services/passager.service';
import { ActivatedRoute } from '@angular/router';
import { Page } from '../../models/page.model';

@Component({
  selector: 'app-view-airfrance',
  standalone: true,
  imports: [FiltresComponent, ListeVolsComponent, ListePassagersComponent, NgIf],
  templateUrl: './view-airfrance.component.html',
  styleUrls: ['./view-airfrance.component.scss']
})
export class ViewAirFranceComponent implements OnInit {
  filtres!: IFiltres;
  vols: Vol[] = [];
  volSelectionne!: Vol;
  typePage: Page = Page.DECOLLAGE;

  constructor(
    private _volService: VolService,
    private _passagerService: PassagerService,
    private _activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this._activatedRoute.url.subscribe(url => {
      this.typePage = (url[0].path === Page.DECOLLAGE ? Page.DECOLLAGE : Page.ATTERRISSAGE);
      console.log(this.typePage);
    });
  }

  recupererFiltresEtRechercherVol(filtres: IFiltres) {
    this.filtres = filtres;
    if (this.typePage == Page.DECOLLAGE) {
      this._volService.getVolsDepart(this.filtres.aeroport.icao, this.filtres.debut.getSeconds(), this.filtres.fin.getSeconds())
        .subscribe(vols => this.vols = vols);
    } else {
      this._volService.getVolsArrive(this.filtres.aeroport.icao, this.filtres.debut.getSeconds(), this.filtres.fin.getSeconds())
        .subscribe(vols => this.vols = vols);
    }
  }

  recupererVolSelectionne(vol: Vol) {
    this._passagerService.getPassagers(vol.icao).subscribe((passagers) => {
      this.volSelectionne = vol;
      if (passagers) {
        this.volSelectionne.passagers = passagers;
      }
    });
  }

  protected readonly Page = Page;
}
