import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Vol } from '../../models/vol.model';
import { VolComponent } from '../vol/vol.component';
import { Page } from '../../models/page.model';

@Component({
  selector: 'app-liste-vols',
  standalone: true,
  imports: [
    VolComponent
  ],
  templateUrl: './liste-vols.component.html',
  styleUrls: ['./liste-vols.component.scss']
})
export class ListeVolsComponent {
  @Input({ required: true }) vols: Vol[] = [];
  @Input() page: Page = Page.DECOLLAGE
  @Output() volSelectionne = new EventEmitter<Vol>();

  selectionnerVol(vol: Vol) {
    this.volSelectionne.emit(vol);
  }
}
