import { Component, EventEmitter, LOCALE_ID, Output, ViewEncapsulation } from '@angular/core';
import { MAT_DATE_RANGE_SELECTION_STRATEGY } from '@angular/material/datepicker';
import { AEROPORTS } from '../../constants/aeroport.constant';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { IAeroport } from '../../models/aeroport.model';
import { ThreeDayRangeSelectionStrategy } from '../../date-adapter';
import { MAT_DATE_LOCALE, provideNativeDateAdapter } from '@angular/material/core';
import {MatCommonModule} from '@angular/material/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { FormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { IFiltres } from '../../models/filtres.model';


@Component({
  selector: 'app-filtres',
  standalone: true,
  templateUrl: './filtres.component.html',
  styleUrls: ['./filtres.component.scss'],
  imports: [MatIconModule, MatButtonModule, MatInputModule,
    MatFormFieldModule, MatSelectModule, MatDatepickerModule, MatCommonModule, CommonModule, ReactiveFormsModule],
  providers: [
    provideNativeDateAdapter(),
    { provide: LOCALE_ID, useValue: 'fr' },
    { provide: MAT_DATE_LOCALE, useValue: 'fr-FR' },
    {
      provide: MAT_DATE_RANGE_SELECTION_STRATEGY,
      useClass: ThreeDayRangeSelectionStrategy,
    },
  ],
  encapsulation: ViewEncapsulation.None
})
export class FiltresComponent {
  @Output() filtres = new EventEmitter<IFiltres>;

  /**
   * La liste des aéroports disponibles est une constante,
   * on n'utilise que les principaux aéroports français pour l'instant
   */
  aeroports: IAeroport[] = AEROPORTS;
  aeroportSelectionne = new FormControl(null, Validators.required);
  dateDebut = new FormControl(null, Validators.required);
  dateFin = new FormControl(null, Validators.required);

  /**
   * Envoie les filtres au composant parent si tous les filtres sont renseignés.
   */
  appliquerFiltres() {
    if (this.aeroportSelectionne.value && this.dateDebut.value && this.dateFin.value) {
      this.filtres.emit({
        aeroport: this.aeroportSelectionne.value,
        debut: this.dateDebut.value,
        fin: this.dateFin.value
      })
    }
  }

  /**
   * Permet de savoir si tous les filtres sont renseignés.
   */
  isFormValid() {
    return this.aeroportSelectionne.valid && this.dateDebut.valid && this.dateFin.valid;
  }
}
