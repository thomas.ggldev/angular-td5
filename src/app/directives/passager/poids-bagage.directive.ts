import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appPoidsBagage]',
  standalone: true
})
export class PoidsBagageDirective implements OnInit {
  @Input({required: true}) classeVol!: string;
  @Input({required: true}) nombreBagageEnSoute!: number

  constructor(private elementRef: ElementRef) {
  }

  ngOnInit() {
    this.checkNombreBagageEnSoute()
  }

  checkNombreBagageEnSoute() {
    if ((this.classeVol === 'STANDARD' && this.nombreBagageEnSoute > 1) ||
      (this.classeVol === 'BUSINESS' && this.nombreBagageEnSoute > 2) ||
      (this.classeVol === 'PREMIUM' && this.nombreBagageEnSoute > 3)) {
      this.elementRef.nativeElement.style.backgroundColor = 'red'
    }
  }
}
