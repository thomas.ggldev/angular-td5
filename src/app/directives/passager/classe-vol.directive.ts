import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appClasseVol]',
  standalone: true
})
export class ClasseVolDirective implements OnInit{
  @Input({ required: true }) classeVol!: string;
  constructor(private elementRef: ElementRef) {
  }

  ngOnInit() {
    this.classeVolColor();
  }

  private classeVolColor() {
    switch (this.classeVol) {
      case 'BUSINESS': {
        this.elementRef.nativeElement.style.color = 'red';
        break;
      }
      case 'PREMIUM': {
        this.elementRef.nativeElement.style.color = 'green';
        break;
      }
      default: {
        this.elementRef.nativeElement.style.color = 'blue';
      }
    }
  }
}
